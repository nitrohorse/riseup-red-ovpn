# [Riseup VPN Red](https://riseup.net/en/vpn/vpn-red)

- Requires [Riseup.net](https://riseup.net/accounts) account via invite code.
- [Deprecated](https://riseup.net/vpn/vpn-red) in favor of [Riseup's Bitmask clients](https://riseup.net/en/vpn#download-riseupvpn) which don't require an account.

## Usage

Example terminal command:
```
$ sudo openvpn --config riseup-tcp-ip-443.ovpn
```

## Options

| Location           | Hostname       | ISP    | Protocols | Ports          | IP/URL |
|--------------------|----------------|--------|-----------|----------------|--------|
| Washington, USA    | vpn.riseup.net | Riseup | TCP/UDP   | 53/80/443/1194 | IP/URL |